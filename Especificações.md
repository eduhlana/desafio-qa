Funcionalidade: Chamada de vídeo 
Descrição: A chamada de video possibilita  que você veja ,ouça ,fale, seja visto e ouvido em tempo real.
Pré-requisitos: Ambos os contatos possuirem um aparelho do tipo smartphone ( De preferência ,que possua câmera frontal),
ter o whatsapp instalado,logado com suas devidas contas e estarem  conectados com a internet.
o realizador da video chamada deve ter o receptor salvo na sua agenda ou  ter recebido uma mensagem do receptor.


Caso de teste:Selecionar o contato pelo número.
Execução: Após tocar e abrir o Whatsapp, siga os passos:tocar na aba contato->tocar no botão de pesquisa->digitar o número do contato
Resultado esperado: O seu contato será retornado na busca.

Caso de teste:Selecionar o contato pelo nome a partir da aba conversas.
Pré-requisitos: Já ter enviado ou recebido uma mensagem do contato requerido, contato salvo com nome.
Execução: Após tocar e abrir o Whatsapp, siga os passos:tocar na aba conversas->tocar no botão de pesquisa->digitar o nome do contato
Resultado esperado: O Seu contato será retornado na busca

Caso de teste: Localizar um contato pelo nome a partir da aba conversas sem que o contato possua nome
Pré-requisitos: Já ter enviado ou recebido uma mensagem do contato requerido, contato salvo sem nome.
Execução: Após tocar e abrir o Whatsapp, siga os passos:tocar na aba conversas->tocar no botão de pesquisa->digitar o nome do contato
Resultado esperado:Devo visualizar a seguinte mensagem:"Sem resultados para 'nome do contato digitado'"

Caso de teste:Realizar uma vídeo chamada a partir do nome do contato.
Pré-requisitos: Realizador e receptor da video chamada conectados a internet, Realizador ter o receptor salvo na agenda de contatos com nome.
Execução: Após tocar e abrir o Whatsapp, siga os passos: tocar na aba contato->tocar no botão de pesquisa->digitar o nome do contato->
Escolha contato(Caso haja mais de um contato com mesmo nome)->
tocar sobre o contato->tocar sobre o icone do telefone que esta ao lado do nome do contato->Apos abrir a tela(pop-up), tocar sobre a opção "chamada de vídeo"
Resultado esperado: Uma chamada de video deverá ser iniciada.            


Gherkin

Feature: Chamada de vídeo
	Descrição: A chamada de video possibilita  que você veja ,ouça ,fale, seja visto e ouvido em tempo real.
	Pré-requisitos: Ambos os contatos possuirem um aparelho do tipo smartphone ( De preferência ,que possua câmera frontal),
	ter o whatsapp instalado,logado com suas devidas contas e estarem  conectados com a internet.
	o realizador da video chamada deve ter o receptor salvo na sua agenda ou  ter recebido uma mensagem do receptor.
	
	Scenario:Selecionar o contato pelo número.
	Given Eu estou na aba "contatos"
	When Eu toco no botao "pesquisar" 
		And Eu digito o numero 'x'
	Then A busca deve retornar o contato correspondente ao numero "x"
	
	Scenario:Selecionar o contato pelo nome a partir da aba conversas.
	Given Eu estou na aba conversas
	When Eu toco no botao "pesquisar" 
		And Eu digito o nome 'contato'
	Then A busca deve retornar o "contato"	
	
	Scenario:Localizar um contato pelo nome a partir da aba conversas sem que o contato possua nome.
	Given Eu estou na aba conversas
	When Eu toco no botao "pesquisar" 
		And Eu digito o nome 'contato'
	Then Eu devo visualizar a mensagem "Sem resultados para 'contato'"
	
	Scenario:Realizar uma vídeo chamada a partir do nome do contato.
	Given Eu estou na aba contatos
	When Eu toco no botao "pesquisar" 
		And Eu digito o nome 'contato'
		And Eu clico no icone do "telefone"
		And Eu clico na opção "Chamada de vídeo"
	Then Una chamada de vídeo deverá ser iniciada.
	




----------------XXXXXX-------------------------xxxxxx----------------------------------XXXXXX---------------------------------------XXXXXXX-------------------








Funcionalidade:Status
Descrição: O status pode ser a descrição de um momento atual em que você esteja , uma reunião por exemplo, ou uma frase que possua um significado especial pra você.
Pré-requisitos: Possuir um aparelho do tipo smartphone, ter o whatsapp instalado,logado com sua devida conta e estar  conectado com a internet.

Caso de teste: Atualizar status
Pré-requisitos:Nenhum outro além dos que ja foram descritos na funcionalidade.
Execução: Após tocar e abrir o Whatsapp, siga os passos: tocar nos 3 pontinhos no canto superior direito do whatsapp->tocar na opção Status->tocar no icone de edição(lapis)->Inserir o novo status->clicar no botão "OK"
Resultado esperado: Devo visualizar uma mensagem "Atualizando seu status" e em seguida ter o status atualizado.

Feature:Status
O status pode ser a descrição de um momento atual em que você esteja , uma reunião por exemplo, ou uma frase que possua um significado especial pra você.
Pré-requisitos: Possuir um aparelho do tipo smartphone, ter o whatsapp instalado,logado com sua devida conta e estar  conectado com a internet.

	Scenario:Atualizar status
	Given Eu estou com o Whatsapp aberto
	When Eu toco nos 3 pontinhos no canto superior direito do whatsapp 
		And Eu toco na opção "Status"
		And Eu toco no icone de edição "lapis"
		And Eu Insiro um "status novo"
	Then Eu devo ver a mensagem "Atualizando seu status"
		And Eu devo ver meu  "status novo"
